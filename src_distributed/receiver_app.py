"""
Receiver App - Distributed flavor
"""
import asyncio
import logging
import json
import time
import argparse

PORT = 5000

def main():
    """ Parses args, create receiver object and starts async tasks """
    parser = argparse.ArgumentParser()
    parser.add_argument("--stats", action='store_true', help="Shows stats")
    args = parser.parse_args()

    receiver_app = ReceiverApp(args.stats)
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(receiver_app.run())
        loop.close()
    except KeyboardInterrupt:
        print("Caught keyboard interrupt. Terminating.")

def get_logger(logger_name):
    """ Creates, configures and returns a logger """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

     # Formatter
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # Console handler
    console_handle = logging.StreamHandler()
    console_handle.setLevel(logging.DEBUG)
    console_handle.setFormatter(formatter)

    logger.addHandler(console_handle)

    return logger

class ReceiverApp:
    """ ReceiverApp class """

    def __init__(self, stats):
        self.logger = get_logger("receiver_app")
        self.stats = stats
        self.counts = {}

    def print_data(self, app_id):
        """ Prints received count """
        print_dict = dict(self.counts[app_id])

        if not self.stats:
            print_dict.pop('min_delay')
            print_dict.pop('max_delay')
            print_dict.pop('total_duration')
            print_dict.pop('start_timestamp')
            print_dict.pop('last_timestamp')
            print_dict.pop('delay_since_last')
            print_dict.pop('failure')

        self.logger.info(print_dict)

    async def send_current_count(self, writer, app_id):
        """ Send current count to sender app """
        self.logger.debug("Sender is requesting current count for appID %s", app_id)
        if app_id in self.counts:
            payload = {'count': self.counts[app_id]['count']}
        else:
            payload = {'count': -1}

        writer.write(json.dumps(payload).encode())
        await writer.drain()

    async def send_refused(self, writer):
        """ Send current count to sender app """
        payload = {'response': 'refused'}
        writer.write(json.dumps(payload).encode())
        await writer.drain()

    def set_counts_dict_defaults(self, app_id, node_id):
        """ Set elements in the appID counts dict to default """
        self.counts[app_id] = {
            'count': 0,
            'app_id': app_id,
            'node_id': node_id,
            'min_delay': None,
            'max_delay': None,
            'delay_since_last': None,
            'total_duration': 0,
            'start_timestamp': None,
            'last_timestamp': 0,
            'failure': None
        }

    async def receiver_app_server(self, reader, writer):
        """ Receiver app server processing data sent from the sender app """
        while True:
            try:
                data = await reader.read(1024)
                if not data:
                    break

                data = json.loads(data.decode())
                app_id = data['app_id']

                # If sender app is requesting current count
                if 'request' in data and data['request'] == 'count':
                    await self.send_current_count(writer, app_id)
                    continue

                current_time = time.time()

                # If received app id doesn't exist in collection or if count == 0
                if not app_id in self.counts or data['count'] == 0:
                    # Set default
                    delay = 0
                    self.set_counts_dict_defaults(app_id, data['node_id'])
                    self.counts[app_id]['start_timestamp'] = current_time
                    self.counts[app_id]['last_timestamp'] = current_time

                # If nodeID went backwards, something is wrong on the sender side. As node_id is used as fence token, received count is refused
                if int(data['node_id']) < int(self.counts[app_id]['node_id']):
                    self.logger.debug("Received count with node_id %s while last count had node_id %s. Refusing data.", data['node_id'], self.counts[app_id]['node_id'])
                    await self.send_refused(writer)
                    continue

                # If count update request, send ok back
                payload = {'response': 'ok'}
                writer.write(json.dumps(payload).encode())
                await writer.drain()

                delay = current_time - self.counts[app_id]['last_timestamp']

                # Set min_delay value
                if self.counts[app_id]['min_delay'] is None:
                    self.counts[app_id]['min_delay'] = round(delay, 3)
                elif delay < self.counts[app_id]['min_delay']:
                    self.counts[app_id]['min_delay'] = round(delay, 3)

                # Set max_delay value
                if  self.counts[app_id]['max_delay'] is None:
                    self.counts[app_id]['max_delay'] = round(delay, 3)
                elif delay > self.counts[app_id]['max_delay']:
                    self.counts[app_id]['max_delay'] = round(delay, 3)

                self.counts[app_id]['delay_since_last'] = round(delay, 3)
                self.counts[app_id]['last_timestamp'] = current_time

                self.counts[app_id]['total_duration'] = round(current_time - self.counts[app_id]['start_timestamp'], 3)

                # Assertions
                # Only update test status if it has not failed before
                if self.counts[app_id]['failure'] is None:

                    # Count has to be +1 from previous count
                    if data['count'] != self.counts[app_id]['count'] + 1 and data['count'] != 0:
                        self.counts[app_id]['failure'] = "Failed count"

                    # If total delay does not match count
                    total_duration = self.counts[app_id]['total_duration']

                    if not total_duration - 0.9 < data['count'] < total_duration + 0.9:
                        self.counts[app_id]['failure'] = "Total duration does not match current count"

                self.counts[app_id]['count'] = data['count']
                self.counts[app_id]['node_id'] = data['node_id']

                self.print_data(app_id)

            except OSError:
                self.logger.warning("Client disconnected")
                break

        writer.close()

    async def listen(self):
        """ Start asyncio streams server """
        server = await asyncio.start_server(self.receiver_app_server, '127.0.0.1', PORT)
        self.logger.info("Now serving on port %s", PORT)
        await server.serve_forever()

    def run(self):
        """ Start asyncio main loop """
        asyncio.run(self.listen())

if __name__ == "__main__":
    main()
