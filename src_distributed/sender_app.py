import asyncio
import logging
import sys
import json
import datetime
import time
import math
import argparse
import random
from enum import Enum

ELECTION_SLEEP_MAX_DELAY = 200

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", help="Node port")
    parser.add_argument("--remote_ip", help="Remote node IP")
    parser.add_argument("--remote_port", help="Remote node port")
    parser.add_argument("--app_id", help="AppID")
    parser.add_argument("--node_id", help="nodeID")
    parser.add_argument("--test")
    args = parser.parse_args()

    # Check arguments
    if not args.port:
        raise ValueError("Missing port argument")
    if not args.remote_ip:
        raise ValueError("Missing remote IP argument")
    if not args.remote_port:
        raise ValueError("Missing remote port argument")
    if not args.app_id:
        raise ValueError("Missing app ID argument")

    sender_app = SenderApp(args.port, args.remote_ip, args.remote_port, args.app_id, args.node_id, args.test)
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(sender_app.run())
        loop.close()
    except KeyboardInterrupt as e:
        print("Caught keyboard interrupt. Terminating.")
    except SystemError as e:
        print(e)

def get_logger(logger_name):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

     # Formatter
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # Console handler
    console_handle = logging.StreamHandler()
    console_handle.setLevel(logging.DEBUG)
    console_handle.setFormatter(formatter)

    logger.addHandler(console_handle)

    return logger

# States used by the state machine
class States(Enum):
    election = 0
    election_sleep = 1
    leader_setup = 2
    leader_idle = 3
    leader_commit = 4
    follower = 5

# Counter commit status
class CounterStatus(Enum):
    committing = 0
    committed = 1

class SenderApp:
    def __init__(self, port, remote_ip, remote_port, app_id, node_id, test):
        self.logger = get_logger('sender_app')
        self.logger.info("Starting Sender App")

        self.port = port
        self.remote_ip = remote_ip
        self.remote_port = remote_port

        self.app_id = app_id
        self.node_id = 1

        self.state = States.election
        self.count = -1
        self.counter_status = CounterStatus.committed
        self.im_leader = False
        self.remote_connected = False # Flag with the expected connection state
        self.next_hearbeat = 0 # Expected delay until next heartbeat
        self.failover = False # Failover is a flag indicating that a failover occured and that we have to take over current commit
        self.test = int(test) if test else None

    async def server(self, reader, writer):
        while True:
            try:
                data = await reader.read(1024)
                if not data:
                    break

                data = json.loads(data)
                if data['query'] == 'leader':
                    self.logger.debug("Remote is asking who's leader")

                    if self.im_leader:
                        self.logger.debug("Replying to remote that I'm the leader")
                        # Calculating next expected heartbeat time
                        now = time.time()
                        delay = math.ceil(now) - now
                        payload = {'response': 'yes', 'next_heartbeat': delay, 'node_id': self.node_id}
                        writer.write(json.dumps(payload).encode())
                        await writer.drain()

                        # Open connection to follower to send heartbeats
                        connected, self.remote_node_reader, self.remote_node_writer = await self.open_connection(self.remote_ip, self.remote_port)
                        self.remote_connected = connected

                    else:
                        if self.state == States.election:
                            self.logger.debug("Election conflict")
                            payload = {'response': 'election'}
                            writer.write(json.dumps(payload).encode())
                            await writer.drain()

                        elif self.state == States.election_sleep:
                            self.logger.debug("Letting remote take leader role")
                            payload = {'response': 'no'}
                            writer.write(json.dumps(payload).encode())
                            await writer.drain()

                        else:
                            self.logger.debug(f"Unexpected {self.state} at this time. Terminating.")
                            sys.exit(1)

                elif data['query'] == 'heartbeat':
                    if self.im_leader:
                        self.logger.debug(f"Got heartbeat but I'm leader. Ignoring it.")
                    else:
                        if data['counter_status'] == 'committing':
                            self.counter_status = CounterStatus.committing
                            self.count = data['count']
                            self.logger.debug(f"Got committing heartbeat with count {self.count}")
                            self.next_hearbeat = 0.1
                            self.heartbeat.set()

                        elif data['counter_status'] == 'committed':
                            self.counter_status = CounterStatus.committed
                            self.count = data['count']
                            self.logger.debug(f"Got committed heartbeat with count {self.count}")
                            self.next_hearbeat = data['next_heartbeat']
                            self.heartbeat.set()

            except OSError as e:
                self.logger.warning("Remote node disconnected")
                writer.close()
                break

        writer.close()

    async def open_connection(self, remote_ip, remote_port):
        # Connect to other node
        fut = asyncio.open_connection(remote_ip, remote_port)
        try:
            # Wait 500ms, then timeout
            reader, writer = await asyncio.wait_for(fut, timeout=0.5)
            return True, reader, writer
        except asyncio.TimeoutError:
            return False, None, None

    async def write_receiver_app(self, payload):
        self.receiver_writer.write(json.dumps(payload).encode())
        await self.receiver_writer.drain()

    async def read_receiver_app(self):
        response = await self.receiver_reader.read(1024)
        return json.loads(response)

    async def write_remote_node(self, payload):
        try:
            self.remote_node_writer.write(json.dumps(payload).encode())
            await self.remote_node_writer.drain()
        except OSError:
            self.logger.debug("Follower seems to be disconnected")
            self.remote_connected = False

    async def read_remote_note(self):
        response = await self.remote_node_reader.read(1024)
        return json.loads(response)

    async def main_loop(self):
        await asyncio.sleep(0.5)
        self.heartbeat = asyncio.Event()
        connected, self.remote_node_reader, self.remote_node_writer = await self.open_connection(self.remote_ip, self.remote_port)
        if connected:
            self.remote_connected = True
            self.logger.info("Connected to remote node")

        while True:
            if self.state == States.election:
                self.logger.debug("Jump to election state")
                await self.election()

            if self.state == States.election_sleep:
                self.logger.debug("Jump to election_sleep state")
                await self.election_sleep()

            if self.state == States.leader_setup:
                self.logger.debug("Jump to leader_setup state")
                await self.leader_setup()

            elif self.state == States.leader_idle:
                self.logger.debug("Jump to leader_idle state")
                await self.leader_idle()

            elif self.state == States.leader_commit:
                self.logger.debug("Jump to leader_commit state")
                await self.leader_commit()

            elif self.state == States.follower:
                self.logger.debug("Jump to follower state")
                await self.follower()

    async def election(self):
        if not self.remote_connected:
            self.logger.info("Not connected to remote node. I'm the leader.")
            self.im_leader = True
            self.state = States.leader_setup

        if not self.im_leader:
            self.logger.info("Requesting leader status to remote node")
            payload = {"query": "leader" }
            await self.write_remote_node(payload)
            response = await self.read_remote_note()

            if response['response'] == 'yes':
                # Remote is leader
                self.next_hearbeat = response['next_heartbeat']
                self.logger.info(f"Expecting next heartbeat in {self.next_hearbeat}s")

                # Leader sends his node_id, my node_id is leader_node_id +1
                self.node_id = response['node_id'] + 1
                self.logger.info(f"Remote is leader. My nodeID is {self.node_id}")
                self.state = States.follower
                return

            elif response['response'] == 'no':
                # Remote is not leader and is letting me know what I'm the leader now
                self.logger.info("Remote is not leader. I'm taking leader role.")
                self.im_leader = True
                self.state = States.leader_setup
                return

            elif response['response'] == 'election':
                self.logger.info("Election conflict")
                self.state = States.election_sleep
                return
            else:
                self.logger.debug(f"Unexpected {self.state} at this time. Terminating.")
                sys.exit(1)

    async def election_sleep(self):
        sleep_duration = random.randint(0, ELECTION_SLEEP_MAX_DELAY) / 1000
        self.logger.info(f"Sleep for {sleep_duration}s before next election")
        await asyncio.sleep(sleep_duration)
        self.state = States.election

    async def leader_setup(self):
        # Connect to receiver app
        fut = asyncio.open_connection('127.0.0.1', 5000)
        try:
            # Wait 200ms, then timeout
            self.receiver_reader, self.receiver_writer = await asyncio.wait_for(fut, timeout=0.2)
            self.logger.info("Connected to Receiver App")
        except asyncio.TimeoutError:
            self.logger.warning("Cannot connect to Receiver App. Terminating.")
            sys.exit(1)

        if self.failover:
            if self.counter_status == CounterStatus.committing:
                    # Leader failed while in committing state
                    # We don't know if commit was done so we have to request current count from receiver
                    payload = {'request': 'count', 'app_id': self.app_id}
                    self.failover = False
                    await self.write_receiver_app(payload)
                    response = await self.read_receiver_app()
                    if response['count'] == self.count:
                        self.logger.debug("Matching counts")
                        self.counter_status = CounterStatus.committed
                        self.state = States.leader_idle
                    else:
                        self.logger.debug("Counts do not match. Committing now.")
                        self.state = States.leader_commit
            else:
                # Leader failed while in committed state, resume current tick count, do not go through idle state
                self.failover = False
                self.state = States.leader_commit
        else:
            self.state = States.leader_idle

    async def leader_idle(self):
        now = time.time()
        next_tick = math.ceil(now)
        sleep_delay = next_tick - now
        await asyncio.sleep(sleep_delay)

        if time.time() < next_tick:
            # Waiting some more time to get to the next time tick
            await asyncio.sleep(next_tick-time.time()+0.05)
        self.state = States.leader_commit

    async def leader_commit(self):
        # If are in committing state, we just failed over, so don't increment as we have to commit previous count
        if not self.counter_status == CounterStatus.committing:
            self.count = self.count + 1

        self.counter_status = CounterStatus.committing

        if self.test == 1 and self.count == 10:
            # Test: fail before starting commit process
            sys.exit(0)

        if self.remote_connected:
            self.logger.info(f"Sending hearbeat with committing status to remote node")
            payload = {'query': 'heartbeat', 'count': self.count, 'counter_status': 'committing'}
            await self.write_remote_node(payload)

        if self.test == 2 and self.count == 10:
            # Test: fail after sending heartbeat but before committing
            sys.exit(0)

        if self.test == 3 and self.count == 10:
            # Test: sender app not responding for 2 seconds before committing
            time.sleep(2)

        payload = {'count': self.count, 'app_id': self.app_id, 'node_id': self.node_id}
        self.logger.info(f"Sending current count {self.count} to Receiver App")
        await self.write_receiver_app(payload)
        response = await self.read_receiver_app()

        if self.test == 4 and self.count == 10:
            # Test: fail after sending commit but before committed heartbeat
            sys.exit(0)

        if self.test == 5 and self.count == 10:
            # Test: sender app not responding for 2 seconds after commit
            time.sleep(2)

        if response['response'] == 'refused':
            # ReceiverApp refused our count. Failover app has taken the leader role over.
            # self.logger.warning(f".")
            raise SystemError("Current count refused by Receiver App. Terminating.")

        if self.remote_connected:
            # Calculating next expected heartbeat time
            now = time.time()
            delay = math.ceil(now) - now + 0.01 # +0.01 used to avoid any rounding / drift errors
            self.logger.info(f"Sending hearbeat with committed status to remote node")
            payload = {'query': 'heartbeat', 'count': self.count, 'counter_status': 'committed', 'next_heartbeat': delay}
            await self.write_remote_node(payload)

        if self.test == 6 and self.count == 10:
            # Test: fail after sending commit and committed heartbeat
            sys.exit(0)

        if self.test == 7 and self.count == 10:
            # Test: sender app not responding for 2 seconds after sending commit and committed heartbeat
            time.sleep(2)

        self.counter_status = CounterStatus.committed
        self.state = States.leader_idle

    async def follower(self):
        fut = self.heartbeat.wait()
        timeout = self.next_hearbeat + 0.2
        try:
            self.logger.info(f"Waiting for next heartbeat. Timeout is {timeout}")
            await asyncio.wait_for(fut, timeout=timeout)
            self.heartbeat.clear()
        except asyncio.TimeoutError:
            self.logger.info("Didn't get heartbeat before timeout")
            self.remote_connected = False
            self.failover = True
            self.state = States.election


    async def run(self):
        self.server = await asyncio.start_server(self.server, '127.0.0.1', self.port)
        await asyncio.gather(
            self.server.serve_forever(),
            self.main_loop(),
        )

if __name__ == "__main__":
    main()