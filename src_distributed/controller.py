import subprocess
import time
import socket
import argparse
import logging
import sys
BASE_PORT = 6000
SENDER_IP = '127.0.0.1'

def get_logger(logger_name):
    """ Creates, configures and returns a logger """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

     # Formatter
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # Console handler
    console_handle = logging.StreamHandler()
    console_handle.setLevel(logging.DEBUG)
    console_handle.setFormatter(formatter)

    logger.addHandler(console_handle)

    return logger

def main():
    logger = get_logger("controller logger")

    parser = argparse.ArgumentParser()
    parser.add_argument("--test", help="test")
    parser.add_argument("--instances", help="Number of app instances to run")
    args = parser.parse_args()

    test = args.test if args.test else None
    app_instances = int(args.instances) if args.instances else 1

    apps = []

    for x in range(1, app_instances + 1):
        app_id = x

        nodeA_port = BASE_PORT + (app_id*2-1)
        nodeB_port = BASE_PORT + (app_id*2)

        apps.append({'nodeA_port': nodeA_port, 'nodeB_port': nodeB_port, 'app_id': app_id})

        start_sender(SENDER_IP, port=nodeA_port, remote_port=nodeB_port, app_id = app_id, test=test)
        time.sleep(2)
        start_sender(SENDER_IP, port=nodeB_port, remote_port=nodeA_port, app_id = app_id)
        time.sleep(0.5)

    while True:
        for app in apps:
            if not check_alive(SENDER_IP, app['nodeA_port']):
                logger.info("%s:%s died", SENDER_IP, app['nodeA_port'])
                start_sender(SENDER_IP, port=app['nodeA_port'], remote_port=app['nodeB_port'], app_id = app['app_id'])
                time.sleep(1)


            if not check_alive(SENDER_IP, app['nodeB_port']):
                logger.info("%s:%s died", SENDER_IP, app['nodeB_port'])
                start_sender(SENDER_IP, port=app['nodeB_port'], remote_port=app['nodeA_port'], app_id = app['app_id'])
                time.sleep(1)

        time.sleep(1)

def start_sender(ip, port, remote_port, app_id, test=None):
    start_cmd = f"start cmd /k python sender_app.py --port {port} --remote_ip {ip} --remote_port {remote_port} --app_id {app_id}"

    if test:
        start_cmd = f"{start_cmd} --test {test}"

    subprocess.Popen(start_cmd, shell=True)

def check_alive(ip, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(1)
    result = sock.connect_ex((ip, port))
    if result == 0:
        return True

    return False

if __name__ == "__main__":
    main()
