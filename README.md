# Failover app
Two versions of the failover app are available: distributed and single.

The distributed uses an approach were 2 instances are running in parallel. The single version only has one instance running at any point in time.

Distributed pros:
- Fast failover, heartbeat based
- Does not rely on an external controller / orchestrator to failover

Distributed cons:
- Requires a fencing token (checked on the receiver app) to avoid duplicates when leader freezes but does not crash
- Based on system clock. NTP sync between nodes might be necessary.

Single pros:
- Single instance running at any point, no risk of having multiple instances sending data at the same time
- Receiver app does not require to check any fencing token or lock

Single cons:
- Requires a storing service to store its current status (count, timestamp, commit_status)
- Requires an external controller to trigger the failover logic. Using complex orchestrators (k8s) might be tricky as reaction time needs to be fast to not miss a tick
# distributed version
The 'distributed' code (src_distributed/) works in distributed leader / follower mode. This provides a fast failover as any missed heartbeat on the follower would immediately trigger an election and if leader is off, follower will take the leader role and start committing.

In order to avoid committing twice, a few things were implemented:
- Hearbeats are not simple heartbeats. They do include leader count and commit status to help the follower know where to resume.
- In case of a failure while leader was already in the committing process, failover has no way to know if the value was sent to receiver. So it has to request that value from the receiver app and check if it matches its local value. If they do match, the new leader can go to idle state. If they don't match, the new leader has to immediately commit the current tick.
- A fencing token was implemented using nodeID to avoid any issue if leader freezes for too long. See description below.
## Fencing token
Our leader sender app and the OS it's running on can freeze at any time, including at the point where the write packet is already in the network stack ready to be sent. So we have to assume that the leader will at some point send a packet while not being the leader anymore. To ensure this doesn't blow the receiver logic, we need to use a fencing token: a token that changes when the roles are changed. Since we already have our handy nodeID, we can use it as fencing token. When a  failover app is started, it gets the leader nodeID and generates its own by incrementing it by one. If at some point the follower takes the leader role, it will not start sending counts with its own nodeID, which is incremented by one compared to the previous leader. If the previous leader tries to send data, the receiver app will detect that it has an "old" nodeID, would refuse it and notify the faulty sender. The faulty sender detecting this will stop.

## States

### election
- Open connection to remote node
- Ask remote if he is the leader
- Wait leader_ask timeout
- if no response: I'm the leader, jump to leader_setup state
- if response is "no", I'm the leader, jump to leader_setup state
- if response is "yes", jump to follower_idle state
- if response is "election", jump to election sleep

### election_sleep
- Wait random delay from up to 100ms
- if remote goes to election: remote is leader, jump to follower_idle
- else: jump to election

### leader_setup
- open connection to Receiver App
- if counter_status = unknown or committing: ask Receiver App
<!-- - if Receiver App counter == local counter: set committed state -->
- set committing state
- jump to leader_idle

### leader_idle
- if status is committing, jump to commit
- wait for next tick
- jump to commit

### leader_commit
- set committing state
- increment counter
- start countdown timer
- send current count with committing status to follower (small response timeout)
- send counter to Receiver App
- send current count with committed status to follower

### follower
- reset heartbeat timeout
- wait heartbeat timeout
- if heartbeat within timeout: restart state
- if not heartbeat within timeout: jump to election
