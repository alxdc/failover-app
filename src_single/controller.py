import subprocess
import time
import socket
import argparse
import os
import logging
import json

SENDER_IP = '127.0.0.1'
BASE_PORT = 6000

def get_logger(logger_name):
    """ Creates, configures and returns a logger """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

     # Formatter
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # Console handler
    console_handle = logging.StreamHandler()
    console_handle.setLevel(logging.DEBUG)
    console_handle.setFormatter(formatter)

    logger.addHandler(console_handle)

    return logger

def main():
    logger = get_logger("controller logger")

    parser = argparse.ArgumentParser()
    parser.add_argument("--test", help="test")
    parser.add_argument("--instances", help="Number of app instances to run")
    args = parser.parse_args()

    test = args.test if args.test else None
    app_instances = int(args.instances) if args.instances else 1

    apps = []

    for x in range(1, app_instances + 1):
        app_id = x
        node_id = 1
        port = BASE_PORT + (node_id-1) * app_instances + app_id

        apps.append({'port': port, 'node_id': node_id, 'app_id': app_id})

        # Create status file
        status_file = open(f'status_{x}.json', 'w')
        status_file.close()

        start_sender(app_id=app_id, node_id=node_id, port=port, test=test)
        time.sleep(3)

    while True:
        # Check node
        for app in apps:
            if not check_alive(SENDER_IP, app['port']):
                logger.info("%s:%s died", SENDER_IP, app['port'])
                app['node_id'] += 1
                app['port'] = BASE_PORT + (app['node_id']-1) * app_instances + app['app_id']

                start_sender(app_id=app['app_id'], node_id=app['node_id'], port=app['port'])
                time.sleep(0.2)

            time.sleep(0.1)

def start_sender(app_id, node_id, port, test=None):
    start_cmd = f"start cmd /k python sender_app.py --app_id {app_id} --node_id {node_id} --port {port}"

    if test:
        start_cmd = f"{start_cmd} --test {test}"

    subprocess.Popen(start_cmd, shell=True)

def check_alive(ip, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(0.05)
    try:
        sock.connect((ip, port))
        payload = json.dumps({'cmd': 'ping'})
        sock.send(payload.encode())
        sock.recv(1024)
        sock.close()
        return True
    except (socket.timeout, ConnectionResetError):
        return False

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt as e:
        # Delete status file
        os.remove("status.json")
        print("Caught keyboard interrupt. Terminating.")
