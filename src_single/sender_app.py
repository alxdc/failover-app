"""
Sender App
"""
import asyncio
import logging
import sys
import json
import time
import math
import argparse
import os
from enum import Enum


def main():
    """ Parses args, create sender object and starts async tasks """
    parser = argparse.ArgumentParser()
    parser.add_argument("--app_id", help="AppID")
    parser.add_argument("--node_id", help="nodeID")
    parser.add_argument("--test", help="nodeID")
    parser.add_argument("--port", help="nodeID")
    args = parser.parse_args()

    # Check arguments
    if not args.app_id:
        raise ValueError("Missing app ID argument")
    if not args.node_id:
        raise ValueError("Missing node ID argument")
    if not args.port:
        raise ValueError("Missing port argument")
    if not args.test:
        test = None
    else:
        test = args.test

    sender_app = SenderApp(args.app_id, args.node_id, args.port, test)

    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(sender_app.run())
        loop.close()
    except KeyboardInterrupt:
        print("Caught keyboard interrupt. Terminating.")
    except SystemError as exception:
        print(exception)

def get_logger(logger_name):
    """ Creates, configures and returns a logger """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

     # Formatter
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # Console handler
    console_handle = logging.StreamHandler()
    console_handle.setLevel(logging.DEBUG)
    console_handle.setFormatter(formatter)

    logger.addHandler(console_handle)

    return logger

# States used by the state machine
class States(Enum):
    """ States used in the state machine """
    leader_setup = 1
    leader_idle = 2
    leader_commit = 3


class SenderApp:
    """ SenderApp class """

    def __init__(self, app_id, node_id, port, test):
        self.logger = get_logger('sender_app')
        self.logger.info("Starting Sender App")

        self.app_id = app_id
        self.node_id = node_id
        self.port = port
        self.state = States.leader_setup
        self.count = -1
        self.failover = False
        self.receiver_reader = None
        self.receiver_writer = None
        self.status_file = f'status_{self.app_id}.json'
        self.tick = 0

        if test:
            self.test = int(test)
        else:
            self.test = 0

    async def server(self, reader, writer):
        """ Server is used to indicate controller that sender app is alive """
        while True:
            try:
                data = await reader.read(1024)
                if not data:
                    break

                payload = json.dumps({'cmd': 'pong'})
                writer.write(payload.encode())

            except OSError:
                self.logger.warning("Disconnected")
                writer.close()
                break

        writer.close()

    async def open_connection(self, remote_ip, remote_port):
        """ Generic open connection method using asyncio streams """
        # Connect to other node
        fut = asyncio.open_connection(remote_ip, remote_port)
        try:
            # Wait 100ms, then timeout
            reader, writer = await asyncio.wait_for(fut, timeout=0.1)
            return True, reader, writer
        except asyncio.TimeoutError:
            return False, None, None

    async def write_receiver_app(self, payload):
        """ Writes data to the receiver app """
        self.receiver_writer.write(json.dumps(payload).encode())
        await self.receiver_writer.drain()

    async def read_receiver_app(self):
        """ Reads data from the receiver app """
        response = await self.receiver_reader.read(1024)
        return json.loads(response)

    def write_status(self, count, timestamp, status):
        """ Stores current status in the status file """
        try:
            with open(self.status_file, "r+") as status_file:
                status_file.truncate(0)
                status_file.seek(0)
                status_file.write(json.dumps({'count': count, 'timestamp': timestamp, 'status': status}))
        except FileNotFoundError:
            self.logger.error("Status file not found. Controller app is probably off. Terminating")
            sys.exit(0)


    def read_status(self):
        """ Reads state from the status file """
        try:
            if os.stat(self.status_file).st_size > 0:
                with open(self.status_file, "r") as status_file:
                    return json.loads(status_file.read())
            else:
                return None
        except FileNotFoundError:
            return None

    async def main_loop(self):
        """ State machine control loop """
        while True:

            if self.state == States.leader_setup:
                self.logger.debug("Jump to leader_setup state")
                await self.leader_setup()

            elif self.state == States.leader_idle:
                self.logger.debug("Jump to leader_idle state")
                await self.leader_idle()

            elif self.state == States.leader_commit:
                self.logger.debug("Jump to leader_commit state")
                await self.leader_commit()

    async def leader_setup(self):
        """ leader_setup state """
        # Connect to receiver app
        fut = asyncio.open_connection('127.0.0.1', 5000)
        try:
            # Wait 200ms, then timeout
            self.receiver_reader, self.receiver_writer = await asyncio.wait_for(fut, timeout=0.2)
            self.logger.info("Connected to Receiver App")
        except asyncio.TimeoutError:
            self.logger.warning("Cannot connect to Receiver App. Terminating.")
            sys.exit(1)

        # Load previous status
        status = self.read_status()
        if status is None:
            self.logger.info("No status, starting app with count 0")
            self.state = States.leader_idle

        else:
            self.count = status['count']
            if math.floor(time.time()) == math.floor(status['timestamp']) and status['status'] == 'committed':
                # Last commit happen in current tick, no need to recommit
                self.logger.info("Previous status was committed, no need to recommit")
                self.state = States.leader_idle

            elif math.floor(time.time()) == math.floor(status['timestamp']) and status['status'] == 'committing':
                # Last commit happen in current tick but still in committing, not sure if commit was properly done, check receiver count
                payload = {'request': 'count', 'app_id': self.app_id}
                await self.write_receiver_app(payload)
                response = await self.read_receiver_app()

                if status['count'] == response['count']:
                    # Receiver count matches local saved count, all good
                    self.logger.info("Counts match, no need to recommit")
                    self.state = States.leader_idle

                else:
                    # Receiver doesn't match local count, recommitting
                    self.logger.info("Counts don't match, recommit")
                    self.failover = True
                    self.state = States.leader_commit

            elif math.floor(time.time()) > math.floor(status['timestamp']):
                # Previous commit is too old, commit asap
                self.logger.info("Previous commit is too old, commit asap")
                self.state = States.leader_commit

        # Leader failed while in committing state
        # We don't know what was the last committed value so we have to request current count from receiver

    async def leader_idle(self):
        """ leader_idle state """
        now = time.time()
        next_tick = math.ceil(now)
        sleep_delay = next_tick - now
        await asyncio.sleep(sleep_delay)

        if time.time() < next_tick:
            # Waiting some more time to get to the next time tick
            await asyncio.sleep(next_tick-time.time()+0.05)
        self.state = States.leader_commit

    async def leader_commit(self):
        """ leader_commit state """

        # If failover, we are in the middle of a committing failover, we have to commit previous count, so don't increment
        if not self.failover:
            self.count += 1

        self.failover = False

        payload = {'count': self.count, 'app_id': self.app_id, 'node_id': self.node_id}
        self.logger.info("Sending current count %s to Receiver App", self.count)

        if self.count == 10 and self.test == 1:
            # Test: fail before starting commit process
            sys.exit(0)

        if self.count == 10 and self.test == 2:
            # Test: Freeze for 2 seconds before starting commit process
            time.sleep(2)
            sys.exit(0)

        # Write committing status to file
        self.write_status(self.count, time.time(), 'committing')

        if self.count == 10 and self.test == 3:
            # Test: fail after storing committing status locally
            sys.exit(0)

        if self.count == 10 and self.test == 4:
            # Test: Freeze for 2 seconds after storing committing status locally
            time.sleep(2)
            sys.exit(0)

        await self.write_receiver_app(payload)
        await self.read_receiver_app()

        if self.count == 10 and self.test == 5:
            # Test: fail after sending data to receiver but before storing it locally
            sys.exit(0)

        if self.count == 10 and self.test == 6:
            # Test: Freeze for 2 seconds after sending data to receiver but before storing it locally
            time.sleep(2)
            sys.exit(0)

        # Write committed status to file
        self.write_status(self.count, time.time(), 'committed')

        if self.count == 10 and self.test == 7:
            # Test: fail after completing the commit process
            sys.exit(0)

        if self.count == 10 and self.test == 8:
            # Test: Freeze for 2 seconds after completing the commit process
            time.sleep(2)
            sys.exit(0)

        self.state = States.leader_idle

    async def run(self):
        """ run is used to start server and gather tasks """
        self.server_task = await asyncio.start_server(self.server, '127.0.0.1', self.port)
        await asyncio.gather(
            self.server_task.serve_forever(),
            self.main_loop(),
        )

if __name__ == "__main__":
    main()
